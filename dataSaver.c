#include<stdio.h>
#include <stdlib.h>

// Decode the data that comes from the post
void decode(char *src, char *last, char *dest)
{
    for(; src != last; src++, dest++)
        if(*src == '+'){
            *dest = ' ';
        }     
        else if(*src == '%') {
            int code;
            if(sscanf(src+1, "%2x", &code) != 1){
            code = '?';
            } 
            *dest = code;     
            src +=2;
        }     
        else{
            *dest = *src;
        }    
        *++dest = '\0';
}

int main(void)
{
    int ID = 0;
    
    char *lenstr;
    char input[100], data[100];
    long len;
    char** tokens; 
    
    char str[10000];
    FILE *fp1, *fp2;
    int del_line_no = 2;
    int line_no = 0;
      
    char s[1000];
    
    time_t t = time(NULL);
    struct tm * p = localtime(&t);
    
    strftime(s, 1000, "%D %T", p);
    
    printf("%s%c%c\n","Content-Type:text/html;charset=iso-8859-1",13,10);
    printf("<TITLE>Response</TITLE>\n");
    lenstr = getenv("CONTENT_LENGTH");
    if(lenstr == NULL || sscanf(lenstr,"%ld",&len)!=1 || len > 80)
        printf("<P>Error in invocation - wrong FORM probably.");
    else {
        fgets(input, len+1, stdin);
        decode(input, input+len, data);

        char* pch;
        char* PostArray[4];
        int i = 0;
        int j = 0;

        pch = strtok (data,"&=");
        while (pch != NULL)
        {
            if((i%2)==1)
            {
                PostArray[j] = pch;
                j++;
            }
            pch = strtok (NULL, "&="); 
            i++;  
        }
    fp1 = fopen("/var/www/html/Opdracht2/data.json", "r");
    rewind(fp1);
    
    fp2 = fopen("/var/www/html/Opdracht2/replica.json", "w");
    ID = atoi (PostArray[0]);
    while(fgets(str,9999, fp1) != NULL)
    {
      line_no++;
      
      if(line_no != ID)
      {
          fprintf(fp2, str);
      }
    }
    fclose(fp1);
    fclose(fp2);
        
    remove("/var/www/html/Opdracht2/data.json");
    rename("/var/www/html/Opdracht2/replica.json", "/var/www/html/Opdracht2/data.json");
    FILE *fp = fopen( "/var/www/html/Opdracht2/data.json" , "a" );
    
    rewind(fp);
    fprintf(fp , " ,{\"datetime\" : \"%s\",\"id\" : \"%s\",\"name\": \"%s\",\"klas\": \"%s\",\"email\": \"%s\"}\0", s, PostArray[0], PostArray[1], PostArray[2], PostArray[3]);
    fprintf(fp , "\r\n]\0");
    fclose(fp);

        // Redirect to your previous page
        const char * redirect_page_format =
        "<html>\n"
        "<head>\n"
        "<meta http-equiv=\"REFRESH\"\n"
        "content=\"0;url=%s\">\n"
        "</head>\n"
        "</html>\n";
        printf (redirect_page_format,"http://localhost/Opdracht2/index.html" );
    }
    return 0;
}
